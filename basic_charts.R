library(tidyverse)
library(scales)
library(hrbrthemes)
library(zoo)

source(file.path("import_tg.R"), encoding = "UTF-8")

dates_div <- 40

combined_daily_cases_graph <- function(data){
max_date <- data$date %>% max()
length_dates <- data$date %>% unique %>% length()

cases_russia <- data %>% filter(type %in% c("cases")) %>%
  filter(name %in% c("Россия", "Москва")) %>% drop_na() %>% arrange(name, type, date) %>% 
  pivot_wider(names_from = name, values_from = cases) %>% 
  mutate(Регионы = Россия - Москва) %>% pivot_longer(cols = c("Россия", "Регионы", Москва), names_to = "name", values_to = "cases") %>%
  group_by(type, name) %>%
  mutate(cases7 = rollmean(cases, k=7, fill = NA, align = "center")) %>% ungroup()

myplot <- cases_russia %>% filter(type == "cases") %>% ggplot(aes(x = date, y = cases, fill = factor(name, levels = c("Москва", "Регионы")))) + 
  geom_bar(data = subset(cases_russia, name %in% c("Москва", "Регионы")), stat="identity", position = "stack", color = "lightgray") +
  geom_line(data = subset(cases_russia, (name %in% c("Россия", "Регионы") & type == "cases")), aes(x = date, y = cases7)) +
  scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
  labs(
  y=element_blank(), 
  x=element_blank(), 
  title= "Число новых заражений COVID-19 по дням в России"#,
  #subtitle = "Черной линией скользящее среднее за 7 дней"
  #caption = paste("tg: @covidcharts. Обновление за", max_date)
  )  + 
  theme_light() + theme(legend.title = element_blank()) + theme(legend.position = c(0.2, 0.7))
return(myplot)
}

diff_graph <- function(data){
  max_date <- data$date %>% max()
  length_dates <- data$date %>% unique %>% length()

  cases_active <- data %>% filter(name == "Россия") %>% drop_na()%>% 
    pivot_wider(names_from = type, values_from = cases) %>% 
    mutate(active = cases - recovered - deaths) %>% arrange(date) %>% drop_na() %>%
    mutate(active7 = rollmean(active, k=7, fill = NA, align = "center"))
  
myplot <-   cases_active %>% ggplot(aes(x = date, y = active, fill = active)) + geom_col()+ 
    scale_fill_gradient(high = "red", low = "green") + 
    geom_line(aes(x = date, y = active7))+
    scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
    scale_y_continuous()+  
    labs(
      y=element_blank(), 
      x=element_blank(), 
      title= "Изменение числа болеющих COVID-19 по дням в России"#,
      #subtitle = subtitle,
      #caption = paste("tg: @covidcharts. Обновление за", max_date)
      )  + 
    theme_light() +  theme(legend.position=c(0.2, 0.8)) + theme(legend.title = element_blank(), 
                                                                legend.text = element_text(size = 6), 
                                                                legend.key.size = unit(0.25, "cm"),
                                                                legend.key.width = unit(0.15,"cm") )
return(myplot)
}

diff_graph_cumsum <- function(data){
  max_date <- data$date %>% max()
  length_dates <- data$date %>% unique %>% length()
  
  cases_active <- data %>% filter(name == "Россия") %>% drop_na()%>% 
    pivot_wider(names_from = type, values_from = cases) %>% 
    mutate(active = cases - recovered - deaths) %>% arrange(date) %>% drop_na() %>%
    mutate(active_cumsum = cumsum(active)) 
  
  myplot <-   cases_active %>% ggplot(aes(x = date, y = active_cumsum, fill = active)) + geom_col()+ 
    #scale_fill_gradient(high = "red", low = "green") + 
    scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
    scale_y_continuous(labels = comma)+
    labs(
      y=element_blank(), 
      x=element_blank(), 
      title= "Число официально болеющих Covid-19 в России",
      #subtitle = subtitle,
      caption = paste("tg: @covidcharts. Обновление за", max_date))  + 
    theme_light() +  theme(legend.position='none') 
  return(myplot)
}

death_simple_chart <- function(data, region){
  if(region == "Россия") {title_text <- "Число умерших от COVID-19 по дням в России"
  } else {title_text <- "Число умерших от COVID-19 по дням в Москве"}
  
  max_date <- data$date %>% max()
  length_dates <- data$date %>% unique %>% length()
  
  death_region<- data %>% filter(name == region, type == "deaths") %>% drop_na() %>% arrange(date) %>%
    mutate(deaths7 = rollmean(cases, k=7, fill = NA, align = "center"))
  mychart <- death_region %>% ggplot(aes(x = date, y = cases)) + geom_col(alpha = 0.7) + 
    geom_line(aes(y = deaths7), size = 1) + 
    scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
    labs(
      y=element_blank(), 
      x=element_blank(), 
      title= title_text,
      #subtitle = subtitle,
      caption = paste("Черная линия обозначает скользящее среднее за 7 дней.\nt.me/covidcharts. Обновление за", max_date))  + 
    theme_light() +  theme(legend.position='none') 
  return(mychart)
}

cases_moscow_graph <- function(data){
  max_date <- data$date %>% max()
  length_dates <- data$date %>% unique %>% length()
  
  cases_moscow <- data %>% filter(name == "Москва", type == "cases")  %>% drop_na() %>% arrange(date)%>%
    mutate(cases7 = rollmean(cases, k=7, fill = NA, align = "center"))
  
  mychart <- cases_moscow %>% ggplot(aes(x = date, y = cases)) + geom_col(alpha = 0.7, fill = "white", color = "gray") + geom_line(aes(y = cases7)) +
    scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
    labs(
      y=element_blank(), 
      x=element_blank(), 
      title= "Новые случаи заражения COVID-19 по дням в Москве"#,
      #subtitle = subtitle,
      #caption = paste("Черная линия обозначает скользящее среднее за 7 дней.\nt.me/covidcharts. Обновление за", max_date)
      )  + 
    theme_light() +  theme(legend.position='none') 
  return(mychart)
}


cases_spb_graph <- function(data){
  max_date <- data$date %>% max()
  length_dates <- data$date %>% unique %>% length()
  
  cases_moscow <- data %>% filter(name == "Санкт-Петербург", type == "cases")  %>% drop_na() %>% arrange(date)%>%
    mutate(cases7 = rollmean(cases, k=7, fill = NA, align = "center"))
  
  mychart <- cases_moscow %>% ggplot(aes(x = date, y = cases)) + geom_col(alpha = 0.7, fill = "white", color = "gray") + geom_line(aes(y = cases7)) +
    scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
    labs(
      y=element_blank(), 
      x=element_blank(), 
      title= "Новые случаи заражения COVID-19 по дням в Санкт-Петербурге",
      #subtitle = subtitle,
      caption = paste("Черная линия обозначает скользящее среднее за 7 дней.\nt.me/covidcharts. Обновление за", max_date)
    )  + 
    theme_light() +  theme(legend.position='none') 
  return(mychart)
}


vent_moscow_graph <- function(data){
#ventilators
max_date <- data$date %>% max()
length_dates <- data$date %>% unique %>% length()
vent_moscow <- data %>% filter(name ==  "Москва", type == "ventilators")%>% drop_na() %>% arrange(date) %>% distinct() %>%
  mutate(cases7 = rollmean(cases, k=7, fill = NA, align = "center"))

mychart <- vent_moscow %>% ggplot(aes(x = date, y = cases)) + geom_col(alpha = 0.7) + #geom_line(aes(y = cases7)) +
  scale_x_date(breaks=pretty_breaks(n = length_dates/dates_div)) +
  labs(
    y=element_blank(), 
    x=element_blank(), 
    title= "Число людей на аппарате ИВЛ в Москве",
    #subtitle = subtitle,
    caption = paste("Черная линия обозначает скользящее среднее за 7 дней.\nt.me/covidcharts. Обновление за", max_date)
  )  + 
  theme_light() +  theme(legend.position='none') 
return(mychart)
}

