library(tidyverse)
library(ggpubr)
theme_set(theme_pubr())

source(file.path("import_tg.R"), encoding = "UTF-8")
source(file.path("basic_charts.R"), encoding = "UTF-8")

#cases_full <- cases_full %>% filter(date > as.Date("2020-09-01"))
options(scipen=10000)
main_chart <- combined_daily_cases_graph(cases_full)


#active_cases <- diff_graph(cases_full)

#active_cases_cumsum <- diff_graph_cumsum(cases_full)

deaths_russia <- death_simple_chart(cases_full, "Россия")

russia_report <- ggarrange(main_chart, #active_cases, 
                           deaths_russia, 
                           ncol = 1, nrow = 2)
russia_report

ggsave("1russia_report.png", height = 12, width = 15)
 
main_moscow <- cases_moscow_graph(cases_full)
vent_moscow <- vent_moscow_graph(cases_full)
main_spb <- cases_spb_graph(cases_full)
#death_simple_chart(cases_full, "Москва")

moscow_report <- ggarrange(main_moscow, main_spb, 
          ncol = 1, nrow = 2)
moscow_report
ggsave("2moscow_report.png", height = 8, width = 12)
  
russialast <- cases_full %>% filter(type == "cases", name == "Россия", date == Sys.Date()) %>% drop_na()
moscowlast <- cases_full %>% filter(type == "cases", name == "Москва", date == Sys.Date()) %>% drop_na()
spblast <- cases_full %>% filter(type == "cases", name == "Санкт-Петербург", date == Sys.Date()) %>% drop_na()
deathlast <- cases_full %>% filter(type == "deaths", name == "Россия", date == Sys.Date()) %>% drop_na()

paste(russialast$cases, "случаев в России, из них", moscowlast$cases, "в Москве и", spblast$cases, "в Санкт-Петербурге. ", deathlast$cases, "смертельных исходов" )
